import io.kotlintest.shouldBe
import io.kotlintest.specs.DescribeSpec
import java.util.*
import kotlin.collections.ArrayList

class Homework2: DescribeSpec({
    describe("Sort list && catch up in time"){
//        context("sort list"){
//            val numList1 : IntArray = intArrayOf(1,2,3,6,7,9,10,11)
//            val numList2 : IntArray = intArrayOf(1,2,8,6,7,9,10,11)
//            val numList3 : IntArray = intArrayOf(5,4,3,2,1)
//            val numList4 : IntArray = intArrayOf(3,1,5,2,7,6)
//            val numList5 : IntArray = intArrayOf(1,3,5,7,9)
//            isSorted(numList1) shouldBe true
//            isSorted(numList2) shouldBe false
//            isSorted(numList3) shouldBe false
//            isSorted(numList4) shouldBe false
//            isSorted(numList5) shouldBe true
//        }

        context("meet the deadline"){
            val qa1 = QA()
            val dev1 = Developer()

            qa1.name = "Serik"
            qa1.department = "QA department"
            dev1.name = "Berik"
            dev1.department = "Software development"

            val list = arrayListOf("task1", "task2", "task3")
            dev1.work(3, list)
            dev1.work(2, list)

            qa1.work(8, list)
            qa1.work(4, list)

            qa1.releaseTesting(2, list) shouldBe true


//            qa1.releaseTesting(2, arrayListOf("registration", "redesign", "addButton")) shouldBe true
//            qa1.releaseTesting(5, arrayListOf("registration")) shouldBe true
//            qa1.releaseTesting(1, arrayListOf("registration", "redesign")) shouldBe true
//            qa1.releaseTesting(3, arrayListOf("registration", "redesign", "addButton", "sessionControl", "saveChanges")) shouldBe true
//            qa1.releaseTesting(3, arrayListOf("registration", "redesign", "addButton", "sessionControl", "saveChanges", "applyChanges", "cancelAction")) shouldBe false
//            qa1.releaseTesting(2, arrayListOf("registration", "redesign", "addButton", "sessionControl")) shouldBe true
//            qa1.releaseTesting(1, arrayListOf("registration", "redesign", "addButton", "sessionControl")) shouldBe false
        }

    }
})

open class Engineer {
    var name: String = ""
    var department: String = ""

    open fun work(capacity: Int, currentTasks: ArrayList<String>) {}
}

class Developer: Engineer(){
    override fun work(capacity: Int, currentTasks: ArrayList<String>){
        // определяем размер массива
        var listSize = currentTasks.size
        for(i in listSize+1..listSize+capacity){
            // добавляем новую задачу в список
            // название задачи заканчивается на число, указывающее порядок в массиве
            currentTasks.add("task$i")
        }
    }
}

class QA : Engineer(){
    var bool = false
    fun releaseTesting(days: Int, list: ArrayList<String>) : Boolean {
        bool = days.toDouble() >= (list.size.toDouble()/2)
        return bool
    }

    override fun work(capacity: Int, currentTasks: ArrayList<String>) {
        // если массив пустой, возвращаемся обратно, так как нечего удалять
        if (currentTasks.isEmpty()) return

        // определяем last index массива
        var index = currentTasks.size-1
        // счетчик для массива
        var counter = capacity

        do {
            // удаляем элементы с конца массива
            currentTasks.removeAt(index)
            index--
            counter--
        } while (currentTasks.size>0 && counter != 0)
    }
}

fun isSorted(list: IntArray) : Boolean{
    var tempList : IntArray = list.copyOf()
    var bool = false
    for (i in 0 until (list.size-1)){
        for (curPos in 0 until (list.size-1)){
            if(list[curPos]>list[curPos+1]){
                var tmp = list[curPos]
                list[curPos] = list[curPos+1]
                list[curPos+1] = tmp
            }
        }
    }
    bool = Arrays.toString(list).equals(Arrays.toString(tempList), true)
    return bool
}