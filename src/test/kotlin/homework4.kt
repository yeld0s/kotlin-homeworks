import io.kotlintest.shouldBe
import io.kotlintest.specs.DescribeSpec

class Homework4 : DescribeSpec({
    describe("Check basic Kotlin implementations"){
        context("Given a string"){
            // method 1: with array sort and loop
            it("All letters in string should be unique, check with sort and loop"){
                uniqueness("Unique") shouldBe true
                uniqueness("unique") shouldBe false
                uniqueness("Un9iqu9e") shouldBe false
                uniqueness("AaBbCcDdEe") shouldBe true
                uniqueness("AaAbcde") shouldBe false
            }

            // method 2: with double loop
            it("All letters in string should be unique, check with double loop"){
                uniqueness2("Unique") shouldBe true
                uniqueness2("unique") shouldBe false
                uniqueness2("Un9iqu9e") shouldBe false
                uniqueness2("AaBbCcDdEe") shouldBe true
                uniqueness2("AaAbcde") shouldBe false
            }
        }

        context("Given two strings"){
            // method 1: with concatenation
            it("Check if one string is rotation of another with concatenation method"){
                rotation("swiswsft","sftswisw") shouldBe true
                rotation("swiswsft","stfswisw") shouldBe false
                rotation("swiSW9sft8","sft8swiSW9") shouldBe true
                rotation("ABACD","CDABa") shouldBe false
                rotation("swift","ftswi") shouldBe true
            }

            it("Check if one string is rotation of another with rotation point"){
                rotation2("swiswsft","sftswisw") shouldBe true
                rotation2("swiswsft","stfswisw") shouldBe false
                rotation2("swiSW9sft8","sft8swiSW9") shouldBe true
                rotation2("ABACD","CDABa") shouldBe false
                rotation2("swift","FTSWI") shouldBe false
                rotation2("swift","ftswi") shouldBe true
            }
        }
    }
})

// with sort and loop
fun uniqueness(str : String) : Boolean {
    var charArr = str.toCharArray()
    var bool = true
    charArr.sort()

    for (i in 0 until charArr.size-1) {
        if(charArr[i] == charArr[i+1]) {
            return false
        }
    }
    return bool
}

// with double loop
fun uniqueness2(str : String) : Boolean {
    var charArr = str.toCharArray()
    var bool = true

    for (i in 0 until charArr.size-2) {
        for(k in i+1 until charArr.size-1){
            if(charArr[i] == charArr[k]) {
                return false
            }
        }
    }
    return bool
}

// method with concatenation
fun rotation(str1: String, str2: String) : Boolean {
    var bool = false
    var charArr1 = str1.toCharArray()
    var charArr2 = str2.toCharArray()
    var concatenatedString = str1+str1

    if(str1.length != str2.length || charArr1.sort() != charArr2.sort()) {
        return false
    }

    if(concatenatedString.contains(str2)){
        return true
    }
    return bool
}

// method with finding rotation point
fun rotation2(str1: String, str2: String) : Boolean {
    var bool = false
    var charArr1 = str1.toCharArray()
    var charArr2 = str2.toCharArray()

    if(str1.length != str2.length) {
        return false
    }

    var index = 0
    var list = arrayListOf<Int>()
    for(i in 0 until str2.length){
        if(charArr1[0] == charArr2[i]){
            list.add(index, i)
            index += 1
        }
    }

    for(i in 0 until list.size) {
        if((str2.substring(list[i])+str2.substring(0, list[i])) == str1){
            return true
        }
    }
    return bool
}



