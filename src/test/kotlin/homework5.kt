import io.kotlintest.shouldBe
import io.kotlintest.specs.DescribeSpec

class Homework5 : DescribeSpec({
    describe("Check if the given number is palindrome or not"){
        // method 1: with array sort and loop
        it("Palindrome or not"){
            isPalindrome(7887) shouldBe true
            isPalindrome(-7887) shouldBe false
            isPalindrome(123321) shouldBe true
            isPalindrome(1122) shouldBe false
            isPalindrome(11) shouldBe true
            isPalindrome(123) shouldBe false
            isPalindrome(11221) shouldBe false
        }
    }
})

fun isPalindrome(num : Int) : Boolean {
    var bool = true
    var n = num
    if(n < 0) return false
    var remainder: Int
    var reversed = 0
    while (n!=0){
        remainder = n % 10
        reversed = reversed*10 + remainder
        n /= 10
    }
    if(num!=reversed) bool = false
    return bool
}