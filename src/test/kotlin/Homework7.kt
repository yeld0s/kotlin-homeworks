import io.kotlintest.matchers.numerics.shouldBeGreaterThan
import io.kotlintest.shouldBe
import io.kotlintest.specs.DescribeSpec

class Homework7 : DescribeSpec({
    describe("Collection.map implementations"){
        val list1 : ArrayList<Int> = arrayListOf(1,2,3)
        val list2 : ArrayList<Int> = arrayListOf(3,5,8)
        val list3 : ArrayList<Int> = arrayListOf(2,2,1)
        val list4 : ArrayList<Int> = arrayListOf(3,2,1)
        val k = 1

        it("Subtraction"){
            operationResult("subtraction", list1, k) shouldBe arrayListOf(0,1,2)
        }
        it("Addition"){
            operationResult("addition", list2, k) shouldBe arrayListOf(4,6,9)
        }
        it("Multiplication"){
            operationResult("multiplication", list3, k) shouldBe arrayListOf(2,2,1)
        }
        it("k should be bigger than 0"){
            k shouldBeGreaterThan 0
        }
        it("Division"){
            operationResult("division", list4, k) shouldBe arrayListOf(3,2,1)
        }

    }
})

fun operationResult(str : String, list : ArrayList<Int>, k : Int) : ArrayList<Int> {
    var result = list
    when (str) {
        "subtraction" -> for(i in result.indices) {
            result[i] -= k
        }
        "addition" -> for(i in result.indices){
            result[i] += k
        }
        "multiplication" -> for(i in result.indices){
            result[i] *= k
        }
        "division" -> for(i in result.indices){
            result[i] /= k
        }
    }
    return result
}