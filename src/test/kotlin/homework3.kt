import io.kotlintest.shouldBe
import io.kotlintest.specs.DescribeSpec
import java.util.*
import kotlin.collections.HashMap

class Homework3: DescribeSpec({
    describe("Checks if the engineer is catching in time with release date before deadline"){
        context("Test for release date"){
            it("Tasks should be completed in given days"){
                val qa1 = QuA()
                val dev1 = Dev()

                qa1.name = "Serik"
                qa1.department = "QA department"
                dev1.name = "Berik"
                dev1.department = "Software development"

                var list1 = hashMapOf("task1" to 1, "task2" to 2, "task3" to 4, "task4" to 2, "task5" to 5)
                println("==============================================================================================")
                println("initial works: $list1")


                dev1.work(3, list1)
                dev1.work(2, list1)
                qa1.work(8, list1)
                qa1.work(4, list1)

                qa1.releaseTesting(2, list1) shouldBe true
                println("==============================================================================================")

            }

        }

    }
})

open class Eng {
    var name: String = ""
    var department: String = ""

    open fun work(capacity: Int, currentTasks: HashMap<String, Int>) {}
}

class Dev: Eng(){
    override fun work(capacity: Int, currentTasks: HashMap<String, Int>){
        var hour : Int
        // HashMap size
        var listSize = currentTasks.size
        var totalHour = 0
        // iterator used for loop over HashMap
        val iterator = currentTasks.iterator()
        var hoursLeft : Int
        val days : Int

        // get sum of hours
        while(iterator.hasNext()){
            val item = iterator.next()
            totalHour += item.value
        }

        // how many working days in total hours
        days = totalHour/8
        // get how many hours left to work
        hoursLeft = 8-(totalHour - (days*8))

        for(i in listSize+1..listSize+capacity){
            hour = (1..8).random()
            // if developer has enough time to work, then we should add this task
            // P.S. Developer can work only 8 hours for one day
            if (hour <= hoursLeft){
                currentTasks["task$i"] = hour
                // after we've added task, we should subtract time required to complete that task from the total hours left to work
                hoursLeft -= hour
            }
        }
        println("after dev's work: $currentTasks")
    }
}

class QuA : Eng(){
    var bool = false
    fun releaseTesting(days: Int, list: HashMap<String, Int>) : Boolean {
        println("final works: $list")
        bool = days.toDouble() >= (list.size.toDouble()/2)
        return bool
    }

    override fun work(capacity: Int, currentTasks: HashMap<String, Int>) {
        // if HashMap is empty return
        if (currentTasks.isEmpty()) return

        var totalHour = 0
        // iterator used for loop over HashMap
        val iterator = currentTasks.iterator()
        val iterator2 = currentTasks.iterator()
        var hoursLeft : Int
        val days : Int

        // get sum of hours
        while(iterator.hasNext()){
            val item = iterator.next()
            totalHour += item.value
        }

        // how many working days in total hours
        days = totalHour/8
        // get how many hours left to work
        hoursLeft = 8-(totalHour - (days*8))

        // counter for iterator
        var counter = capacity

        // same logic as in developer, but with removing instead of adding
        while(iterator2.hasNext()){
            var item = iterator2.next()
            if (counter == 0) break
            if (item.value<=hoursLeft){
                counter -= 1
                hoursLeft -= item.value
                iterator2.remove()
            }
        }
        println("after Qa's work: $currentTasks")
    }
}

fun IntRange.random() = Random().nextInt((endInclusive + 1) - start) +  start