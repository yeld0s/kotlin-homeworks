import io.kotlintest.shouldBe
import io.kotlintest.specs.DescribeSpec
import java.util.*

class Homework1 : DescribeSpec({
    describe("Checks on basic Kotlin implementations"){
        context("check regions"){
            region(9) shouldBe "Karagandy"
        }

        context("duplicate strings in array"){
            it("check if array has duplicates"){
                val fruits = arrayListOf("Apple", "Banana", "Orange", "Apple", "Mandarin")
                hasDuplicates(fruits) shouldBe true
            }
        }

        context("sum of elements"){
            val listOfNumber = listOf(1,2,3,4)
            getSum(listOfNumber) shouldBe 10
            listOfNumber.sum() shouldBe 10
        }

        context("sort list"){
            val numList : IntArray = intArrayOf(1,2,3,6,7,9,10,11)
            bubbleSort(numList) shouldBe true
        }
    }
})

fun region(obj: Any):String =
        when (obj) {
            1 -> "Astana"
            2 -> "Almaty"
            "Z" -> "Astana"
            "A" -> "Almaty"
            9 -> "Karagandy"
            "M" -> "Karagandy"
            else -> "no region found"
        }

fun hasDuplicates(list: ArrayList<String>) : Boolean {
    var bool = false
    for (i in 0 until list.size){
        for (k in i+1 until list.size){
            if (i!=k && list.elementAt(i).equals(list.elementAt(k)) ){
                bool = true
            }
        }
    }
    return bool
}

fun getSum(list: List<Int>) : Int {
    var sum = 0
    for (item in list){
        sum+=item
    }
    return sum
}

// BubbleSort
fun bubbleSort(list: IntArray) : Boolean{
    var tempList : IntArray = list.copyOf()
    var bool = false
    for (i in 0 until (list.size-1)){
        for (curPos in 0 until (list.size-1)){
            if(list[curPos]>list[curPos+1]){
                var tmp = list[curPos]
                list[curPos] = list[curPos+1]
                list[curPos+1] = tmp
            }
        }
    }
    if (Arrays.toString(list).equals(Arrays.toString(tempList), true)){
           bool = true
    }
    return bool
}

