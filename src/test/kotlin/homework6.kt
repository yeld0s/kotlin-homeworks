import io.kotlintest.matchers.numerics.shouldBeGreaterThan
import io.kotlintest.matchers.numerics.shouldBeGreaterThanOrEqual
import io.kotlintest.matchers.string.shouldContain
import io.kotlintest.specs.DescribeSpec

class Homework6 : DescribeSpec({
    describe("Check for the binary addition & swap without using third variable"){

        var str1 = "111111111101"
        var str2 = "1111111111"
        var str3 = "00010"
        var str4 = "00101"
        var a = 0
        var b = 1
        var c = 3
        var d = 11

        it("Numbers should be binary"){
            str1.shouldContain("^[0-1]*$".toRegex())
            str2.shouldContain("^[0-1]*$".toRegex())
            str3.shouldContain("^[0-1]*$".toRegex())
            str4.shouldContain("^[0-1]*$".toRegex())
        }
        it("Print result of addition"){
            print("$str1 + $str2 = ${binaryAddition(str1,str2)}").also { println() }
            print("$str3 + $str4 = ${binaryAddition(str3,str4)}").also { println() }
        }
        it("Numbers should be positive"){
            a shouldBeGreaterThanOrEqual 0
            b shouldBeGreaterThanOrEqual 0
            c shouldBeGreaterThanOrEqual 0
            d shouldBeGreaterThanOrEqual 0
        }
        it("Swap without third variable"){
            print("before swap: ").also { println("number1 = $a and number2 = $b") }
            print("after swap: ").also { println("${swap(a, b)}") }
            print("before swap: ").also { println("number1 = $c and number2 = $d") }
            print("after swap: ").also { println("${swap2(c, d)}") }
        }
    }
})

fun binaryAddition(bin1: String, bin2: String): String {
    var a = bin1
    var b = bin2
    var finalString = ""
    var result = arrayListOf<Int>()

    if(a.length>b.length){
        var c = a.length-b.length
        while(c>0) {
            b = "0$b"
            c -= 1
        }
    } else if(a.length<b.length){
        var c = b.length-a.length
        while(c>0) {
            a = "0$a"
            c -= 1
        }
    }
    var chr1 = a.toCharArray()
    var chr2 = b.toCharArray()

    var index: Int = if(chr1.size > chr2.size) chr1.size-1 else chr2.size-1
    var remainder = 0
    var counter = 0

    while(index >= 0){
        var a : Int = if(counter >= chr1.size) 0 else chr1[index].toString().toInt()
        var b : Int = if(counter >= chr2.size) 0 else chr2[index].toString().toInt()

        var temp = a + b + remainder
        if(temp == 0 || temp == 1){
            result.add(temp)
            remainder = 0
        } else if(temp == 2){
            result.add(0)
            remainder = 1
        } else if(temp == 3){
            result.add(1)
            remainder = 1
        }
        index -= 1
        counter += 1
    }
    if(remainder!=0) result.add(remainder)
    result.reverse()
    for (i in 0 until result.size) {
        finalString += "${result.get(i)}"
    }
    return finalString
}

fun swap(a : Int, b : Int) : String{
    var n1 = a
    var n2 = b
    n1 += n2
    n2 = n1-n2
    n1 -= n2
    return "number1 = $n1 and number2 = $n2"
}

fun swap2(a : Int, b : Int) : String {
    var n1 = a
    var n2 = b
    n1 = n2.also { n2 = n1 }
    return "number1 = $n1 and number2 = $n2"
}